import { Moment } from './moment';

export interface MomentResponse {
  error: string;
  openingstijden_url: string;
  moments: Moment[];
}
