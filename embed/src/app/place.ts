export interface Place {
  id: string;
  latitude: number;
  longitude: number;
  name: string;
  streetname: string;

  // distanceFromCenter can be calculated client-side after we have retrieved a
  // list of places.
  distanceFromCenter: number;
}
