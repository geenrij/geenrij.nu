import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';

import { PlacesResponse } from './places_response';
import { MomentResponse } from './moment_response';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MomentjeService {
  private baseURL: string;

  constructor(
    private http: HttpClient
  ) {
    if (location.hostname.includes('localhost')) {
      this.baseURL = '//localhost:8080';
    } else if (location.hostname.includes('staging')) {
      this.baseURL = 'https://momentje.staging.geenrij.nu';
    } else {
      this.baseURL = 'https://momentje.geenrij.nu';
    }
  }

  listPlaces(zipcode: string, housenumber: string): Observable<PlacesResponse> {
    return this.http.post<PlacesResponse>(this.baseURL + '/v2/places', { zipcode, housenumber });
  }

  findMoment(zipcode: string, housenumber: string, placeID: string): Observable<MomentResponse> {
    return this.http.post<MomentResponse>(this.baseURL + '/v2/moment', { zipcode, housenumber, place_id: placeID });
  }
}
