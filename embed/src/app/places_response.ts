import { Place } from './place';

export interface PlacesResponse {
  error: string;
  center_latitude: number;
  center_longitude: number;
  places: Place[];
}
