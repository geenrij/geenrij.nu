import { Component } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

import { MomentjeService } from './momentje.service';
import { Place } from './place';
import { Moment } from './moment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public checkoutForm: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    public momentjeService: MomentjeService
  ) {
    this.checkoutForm = this.formBuilder.group({
      zipcode: '',
      housenumber: ''
    });

    const urlParams = new URLSearchParams(window.location.search);
    this.customBackgroundColor = '#' + urlParams.get('bg-color');
  }

  public customBackgroundColor = '#fff';

  // global
  public loading: boolean;
  public phase = 'enter_form';

  // location
  public formError: string;

  // place
  public places: Place[];

  // moment
  public momentTexts: string[] = [];
  public momentVolledigeOpeningstijdenURL = '';
  public momentError: string;

  public onSubmit(formData) {
    if (formData.zipcode === '') {
      this.formError = 'Postcode mist';
      return;
    }
    if (formData.housenumber === '') {
      this.formError = 'Huisnummer mist';
      return;
    }

    this.loading = true;
    this.momentjeService.listPlaces(formData.zipcode, formData.housenumber).subscribe((placesResponse) => {
      if (placesResponse.error !== '') {
        this.formError = placesResponse.error;
      } else {
        this.places = placesResponse.places;
        this.phase = 'choose_place';
        this.places.forEach(place => {
          place.distanceFromCenter = this.calculateDistanceBetweenTwoGeo(
            placesResponse.center_latitude,
            placesResponse.center_longitude,
            place.latitude,
            place.longitude,
          );
        });
        this.places.sort((a, b) => a.distanceFromCenter - b.distanceFromCenter);
      }
      this.loading = false;
    });
  }

  public onSelectPlace(placeID: string) {
    this.loading = true;
    this.momentjeService.findMoment(
      this.checkoutForm.value.zipcode,
      this.checkoutForm.value.housenumber,
      placeID
    ).subscribe((momentResponse) => {
      if (momentResponse.error !== undefined && momentResponse.error !== '') {
        console.log('error');
        this.momentError = momentResponse.error;
      } else {
        this.momentVolledigeOpeningstijdenURL = momentResponse.openingstijden_url;
        const now = new Date();
        let presentedMomentsCount = 0;
        momentResponse.moments.some((moment) => {
          const momentDatetime = new Date(moment.datetime);

          const hours = momentDatetime.getHours();
          const minutes = momentDatetime.getMinutes();
          const momentText = this.daysToName(momentDatetime) + ' om ' + hours + ':' + (minutes < 10 ? '0' + minutes : minutes);
          this.momentTexts.push(momentText);

          presentedMomentsCount++;
          if (presentedMomentsCount === 3) {
            // quit converting dates when we have three viable dates.
            return true;
          }
          return false;
        });
      }
      this.phase = 'show_moment';
      this.loading = false;
    });
  }

  public daysToName(date: Date): string {
    const datesAreOnSameDay = (first: Date, second: Date) =>
      first.getFullYear() === second.getFullYear() &&
      first.getMonth() === second.getMonth() &&
      first.getDate() === second.getDate();
    const dayName = (d: Date) => {
      switch (d.getDay()) {
        case 0: return 'zondag';
        case 1: return 'maandag';
        case 2: return 'dinsdag';
        case 3: return 'woensdag';
        case 4: return 'donderdag';
        case 5: return 'vrijdag';
        case 6: return 'zaterdag';
      }
      return '';
    };
    const addDayToDate = (d: Date): Date => {
      const extraDay = 24 * 60 * 60 * 1000;
      return new Date(d.setTime(d.getTime() + extraDay));
    };

    let comparisonDate = new Date();

    if (datesAreOnSameDay(date, comparisonDate)) {
      return 'Vandaag';
    }

    comparisonDate = addDayToDate(comparisonDate);
    if (datesAreOnSameDay(date, comparisonDate)) {
      return 'Morgen (' + dayName(date) + ')';
    }

    comparisonDate = addDayToDate(comparisonDate);
    if (datesAreOnSameDay(date, comparisonDate)) {
      return 'Overmorgen (' + dayName(date) + ')';
    }
    return date.getDate() + '-' + (date.getMonth() + 1) + ' (' + dayName(date) + ')';
  }

  private calculateDistanceBetweenTwoGeo(lat1: number, lon1: number, lat2: number, lon2: number): number {
    const R = 6371; // km (change this constant to get miles)
    const dLat = (lat2 - lat1) * Math.PI / 180;
    const dLon = (lon2 - lon1) * Math.PI / 180;
    const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
      Math.sin(dLon / 2) * Math.sin(dLon / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    const d = R * c;
    return d;
  }
}
