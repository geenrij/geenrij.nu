#!/bin/bash

defaultEnv="staging"
echo -n "Which environment to deploy to? (${defaultEnv}): "
read -r env
if [ -z "$env" ]
then
	env=$defaultEnv
fi

set -euxo pipefail

(
	cd embed
	ng build
)

rm -rf public/embed
cp -rf embed/dist/embed public/embed

# Deploy
firebase --project "s11-geenrij-${env}" deploy --public "public"
